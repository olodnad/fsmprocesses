{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "process"
, dependencies =
    [ "formatters"
    , "numbers"
    , "console"
    , "dotlang"
    , "effect"
    , "psci-support"
    , "halogen"
    , "debug"
    , "svg-parser-halogen"
    ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
