\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}
%

\usepackage{tikz}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{xcolor}
\usepackage{stmaryrd}
\usepackage{url}
% See "https://tex.stackexchange.com/a/106719" for an explanation of the following line.
\SetSymbolFont{stmry}{bold}{U}{stmry}{m}{n}
\usepackage{listings}
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={PureScript: Processes.Form.Types},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true}
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}



\begin{document}
    \title{Modeling Business Processes and Contractual Agreements with Extended Finite State Machines\thanks{Supported by Innosuisse.}}
    %
    \titlerunning{ }
    % If the paper title is too long for the running head, you can set
    % an abbreviated paper title here
    %
    \author{%
        Dandolo Flumini\inst{1}
        \and
        Nicolas Gagliani\inst{1}
        \and
        Joris Morger\inst{1}
        \and
        Christoph Auth\inst{1}
        \and
        Reto Husmann\inst{3}
        \and Rolf Günter\inst{2}
    }
    %
    \authorrunning{D. Flumini et al.}
    % First names are abbreviated in the running head.
    % If there are more than two authors, 'et al.' is used.
    %
    \institute{%
        ZHAW Zurich University of Applied Sciences, School of Engineering\\
        \url{http://www.zhaw.ch}\\
        \email{\{flum,gagl,morr,auth\}@zhaw.ch}
        \and
        ZHAW Zurich University of Applied Sciences, School of Management and Law, Center for Risk \& Insurance \\
        https://www.zhaw.ch/en/sml/institutes-centres/iri/\\
        \email{gueo@zhaw.ch}
        \and
        Alabus AG, Birchstrasse 189, 8050 Zürich, Switzerland\\
        \email{reto.husmann@alabus.com}
    }
    %
    \maketitle              % typeset the header of the contribution
    %
    \begin{abstract}
        The present work introduces a method to model business processes and agreements based on finite state machines (with additional memory). The paper includes a trace semantics that captures a notion of all possible interactions under the rules of a given process. Besides the general model, the present paper also introduces a derived model (the $TC$-model). The $TC$-model is less expressive than the general model but offers a concise and intuitive way to describe processes. Based on the $TC$-model,  the paper provides a notation to represent processes graphically as well as an implementation of a simple process simulator.

        \keywords{Business Process Modelling \and Contract Languages \and Finite State Machines.}
    \end{abstract}
    %
    %
    %
    \section{Introduction}
    The present work emerged in the context of ``DIGIM''\footnote{Digitales Immobiliendossier, Innosuisse (Innovationsprojekt / Projekt Nr. 30016.1 IP-ICT).}, a collaboration project between the Zurich University of Applied Sciences and Alabus AG. The overall project aim is the development of a decentralized digital real-estate platform without the need for a centralized database. One of the platform's features is the provision of components that permit the specification and evaluation of processes and agreements (e.g., trade and property transfer processes, mortgage agreements). The present work is about the theoretical foundation and specification of these components.

    One of the key design challenges was to specify a framework that is capable of simultaneously accommodating various groups of potential users; companies (such as banks, insurances, and real-estate agencies), the office of deeds, private home-owners and potentially many more in the future. The readiness to abandon manual processes in favor of automated workflows and ``smart contracts'' varies greatly not only between different user groups but also within these groups.  Therefore, the present system separates the formal specification of processes and agreements from their automation. In our approach, specifications of processes and interactions are stored on the platform, while individual users keep sovereignty about how to automatize the background workflows. The system, however, is amenable to later extensions that include components to (separately) implement automation tasks directly on the platform.

    The presented model is an extension of finite state machines with a specific semantics that fits our purpose. The semantics is in terms of ``interaction sequences'', which are traces that represent the interaction of several participants following a process. We do not distinguish between contractual agreements and (business) processes. Rather, we model abstract entities that specify (and regulate) the set of possible interactions between participating users. Whether to regard the artifacts as processes or contracts, respectively, is more a question of how the surrounding platform enforces the specified interactions, and about how individual participants consent to participate in a given process. These modalities pertain to the overall platform and are not part of the formalism presented here.

    \subsection{Related Work}
    There is a rich body of literature on formal modeling of business processes and contractual agreements.

    Existing business processes models (and their semantics) are based on process calculi (e.g.,\cite{workflowPi}), process algebras (e.g.,\cite{processAlgebra}), Petri nets (e.g.,\cite{bpmnPetri3}), finite state machines (e.g.,\cite{bpmnSmalldbFSA}), and various other systems. The motivation and focus of existing works are various; process documentation and specification, process analysis and optimization, or the evaluation of processes in a live environment.

    The field of (formal) contract modeling has attained increasing interest since the advent of Blockchain and smart contracts platforms.  While many Blockchain platforms are built around Turing complete languages with the ``look and feel'' of general-purpose programming languages (e.g., solidity on the Ethereum platform), some developments focus on more specific language designs that reflect on key properties of Blockchain systems (e.g., security, amenability to formal verification and understandability, cf. \cite{smartContractSurvey}, \cite{simplicity}). Aside from Blockchain specific developments, contract languages and formalisms are often based on specifically implemented datatypes (mostly for domain-specific contracts e.g., \cite{jonesEber} for financial contracts), on deontic logic (e.g., \cite{bclContractLang}, \cite{contractAutomata16}), finite state machines (e.g.\cite{contractAsAutomaton}, \cite{contractAutomata16}) or various other notions. The semantics of the proposed formalisms range from valuation and pricing (e.g., \cite{jonesEber}) to compilation to existing interaction platforms (e.g., \cite{fsaBitcoin} for Bitcoin and \cite{henglein} for Ethereum).

    Following the requirements of the ``DIGIM'' platform, our model aims at capturing (a meaningful portion) of business processes and contractual agreements simultaneously. From a technical and ``philosophical'' point of view, our approach is similar to the work presented in \cite{henglein}. Aside from also being based on finite state machines, we also adhere to the idea of separating ``contractual terms'' from  ``contract execution'', as is carefully done in \cite{henglein}. In fact, our present model completely singles out workflow automation and instead focuses on the specification and interpretation interactions. In that sense, artifacts of our model roughly correspond to contractual terms in \cite{henglein}, and automation is performed independently by each user. Future iterations of our approach may, however, include components similar to ``contract managers'' of \cite{henglein} to automate (some of) the background work directly on the ``DIGIM'' platform. While \cite{henglein} presents a ``compilation'' to an existing Blockchain system (Ethereum), our semantic output (interaction sequences) is general but also suitable to be stored in an append-only data structure.

    \section{The Abstract Process Model}
    A fundamental conceptual model of a (business) process is that of an object that undergoes specific internal (state-) changes while a set of agents interacts with it. Abstractly, such a model can be captured (for the most part) by a function
    \begin{align*}
    \delta: S\times M\to S
    \end{align*}
    where $S$ denotes some universal set of states, and elements of $M$ represent messages or ``interactions'' that, depending on the current state, trigger specific changes within the process. In this basic sense, a process can be regarded as a finite state machine. However, most real-world processes rely on a bidirectional flow of information, for instance, they may require a user to take a specific action, they may trigger secondary (sub-) processes or they may inform an agent about the possible interactions that are available at any given point in time. Moreover, this kind of information exchange almost always relies on data accumulated during previous interactions.
    For example, a process designed to handle hardware orders in a company may require approval for certain requests and therefore is likely to store details of orders (e.g. name and role of the employee requesting the new hardware).
    Although it is possible, in theory, to encode such information in (the states of) a finite state machine, designing processes in that way would require constant adjustment of processes; for example, if a new employee is hired. We seek a formalism that is robust under these kinds of changes in the environment. Thus, we define abstract processes essentially as Mealy automata that are equipped with an associative array to store relevant data.


    \subsection{Specification of the Model}\label{sc:modelSpec}
    \begin{definition}[Associative Array]
        Let $K$ be a finite set (of keys) and let $V$ be an arbitrary set of values. An abstract associative array with keys in $K$ is a mapping $A:K\to {2}^V$ (where $x\mapsto 2^x$ denotes the powerset operator). An array-state\footnote{The mere term ``state'' is avoided here to prevent confusion with the state of an abstract process that will be introduced later.} of $A$ is a right-unique relation $s\subseteq K\times V$ such that every $(k,v)\in s$ satisfies $v\in A(k)$. We write $S_A$ to mean the set of all array-states of a given associative array $A$. Furthermore, we assume the following standard operations to interact with array-states of any given associative array $A$ with keys in $K$.:
        \begin{align*}
        & \mathsf{lookup}:K\times S_a\to \{\bot\}\cup V \\
        & \mathsf{lookup}(k,s)=
        \begin{cases}
        v    &\text{if } (k,v)\in s\\
        \bot &\text{if } k\notin dom(s)
        \end{cases}\\
        &\mathsf{insert}:K\times V\times S_A\to \{\bot\}\cup S_A\\
        &\mathsf{insert}(k,v,s) = (k,v)\oplus s
        \end{align*}
        where
        \begin{align*}
        (k,v)\oplus s =
        \begin{cases}
        \{(x,y)\in s\mid x\neq k \}\cup \{(k,v)\} & \text{if } v\in A(k)\\
        \bot                               &\text{otherwise.}
        \end{cases}
        \end{align*}
        Given two array-states $s_1$ and $s_2$ of $a$, the function $\mathsf{insert}$ can be iterated to merge array-states.
        \begin{align*}
        \mathsf{merge}(s_1,s_2) =
        \begin{cases}
        s_2&\text{if } s_1=\varnothing\\
        \mathsf{merge}(s_1\setminus\{x\},\mathsf{insert}(x,s_2))&\text{for some } x\in s_1.
        \end{cases}
        \end{align*}
        Note that the order in which elements from $s_1$ are inserted into $s_2$ does not matter because array-states are right unique.
    \end{definition}

    \begin{remark}
        To specify an associative array $a$, we will also use the notation
        \begin{align*}
        a = x_1:y_1,\dots, x_n:y_n
        \end{align*}
        to mean
        \begin{align*}
        a = \{(x_1,y_1),\dots, (x_n,y_n)\}.
        \end{align*}
    \end{remark}

    Now we define abstract processes as Mealy automata equipped with an associative array. The idea is that the state transition function acts as an interface between agents that interact with a process on one side, and the internals of the process on the other side. The transition function accepts inputs of the form $(q,s,c)$ where $q$ is an abstract state, $s$ is an array-state, and $c$ is a ``commit-message'' sent to the process. Collectively, the elements $q$ and $s$ represent the ``total internal state'' of the process, while the commit-message $c$ represents an interaction meant to trigger a certain internal state update. The partialness of the state transition function is instrumental for the modeling of processes that disallow the triggering of certain transitions under certain circumstances. For instance, if a transition is only supposed to be triggered by a specifically authorized group of users, then the state transition function will be undefined (meaning unavailable) for commit-messages coming from the wrong type of sender\footnote{Here we assume that commit messages themselves carry information about the sender.}.

    \begin{definition}[Abstract Process]
        An abstract process is a tuple
        \begin{align*}
        (Q, A, C, M, \delta, q_{init},F)
        \end{align*}
        where,
        \begin{itemize}
            \item $Q$ is a finite set of states
            \item $A$ is an associative array
            \item $C$ is a set of possible commit-messages
            \item $M$ is a set of messages
            \item $\delta$ is a partial function
            \begin{align*}
            \delta:Q\times S_A\times C \to Q \times S_A\times M
            \end{align*}
            \item $q_{init}\in Q$ is the \textit{initial} state
            \item $F\subseteq Q$ is a set of (possible) final states.
        \end{itemize}
    \end{definition}

    \begin{remark}
        In the literature about finite state machines and their semantics, sets equivalent to the set of commit-messages $C$ and the set of messages $M$ are sometimes called input-set and output-set, respectively (e.g., \cite{sumitMoshe09}). We decided to use the present naming convention to emphasize that an input is an interaction \textit{committed} by an external agent, and that the output is a message representing an unspecified effect in the context of the process (e.g., writing to a database or sending an actual message to some user).
    \end{remark}

    \begin{remark}
        In the following, if it is clear from the context that we refer a formal representation, we may sometimes write ``process'' to mean an abstract process as defined above.
    \end{remark}

    The semantic notion that we introduce along with processes is that of an ``interaction sequence'', a description of a process' behavior under a given sequence of commit-messages. It is straightforward to derive trace notions that describe ``observable parts'' of interactions in terms of projections that determine the elements that are visible to (various kinds of) observers.

    \begin{definition}[Interaction Sequence]\label{def:interactionSequence}
        Let $P$ be the process
        \begin{align*}
            P=(Q, A, C, M, \delta, q_{init},F).
        \end{align*}
        Given a finite sequence $(c_i)_{i<n}$ of commits, we define a natural number $n_0\leq n$ and sequences $(q_i)_{i<n_0}$ of states, $(s_i)_{i<n_0}$ of array-states and $(m_i)_{i<n_0}$ by the base case
        \begin{align*}
            q_0 &= q_{init} &  s_0&=\varnothing,
        \end{align*}
        the recursive step\footnote{We use the notation $(x,y,z)_1=x$, $(x,y,z)_2=y$ and $(x,y,z)_3=z$.}
        \begin{align*}
            q_{k+1}&=\delta(q_k,s_k,c_k)_1 & s_{k+1}&=\delta(q_k,s_k,c_k)_2 &m_k = \delta(q_k,s_k,c_k)_3,
        \end{align*}
        and $m_k=\delta(q_k,s_k,c_k)_3$. The recursion is carried on for as long as $\delta (q_k,s_k,c_k)$ is defined, i.e. $n_0 = \min\{k\in\mathbb{N}\mid \delta(q_k,s_k,c_k)=\bot\}$. Given a sequence $(c_i)_{i<n}$ of commit-messages, we call the sequence $(c_i, q_i,s_i,m_i)_{i<n_0}$ the interaction sequence associated with $(c_i)_{i<n}$. A sequence is called interaction sequence if it is the interaction sequence associated with some sequence of commits.
    \end{definition}
    \begin{remark}
        Subsequently, we will represent interaction sequences of the form $(c_i, q_i,s_i,m_i)_{i<n_0}$ as follows:
        \begin{align*}
        q_0,s_0\xrightarrow{c_0\: m_0}
        q_1,s_1\xrightarrow{c_1\: m_1}
        \dots
        \end{align*}
    \end{remark}


    \begin{remark}
        Interaction sequences are deterministic in the sense that for every sequence of commit-messages induces at most one interaction sequence associated with the given history of commits. However, most processes accommodate different interaction sequences.
    \end{remark}

    \begin{remark}
        Further interpretation of interaction sequences is purposedly left open at this point. Depending on the context and environment, several interpretations are possible. For instance, in the context of ``contractual agreements,'' an interaction sequence may represent a proof that a given contract has been satisfied (or breached). The commit-messages in such a scenario would constitute the atomic parts of such proof.
    \end{remark}



    \section{The Transition-Commit Model}\label{sc:concrete}
    To specify a process, all components $Q$, $A$, $C$, $M$, $q_0$,  $F$, as well as a specification of a function $\delta$ must be provided. The abstract process model does not impose any restrictions on either of these components. For instance, the associative array of a process may have infinitely many states (e.g., if $A(k)=\mathbb{N}$ for some key $k$). Thus, the state transition function may, for example, nontrivially depend on the array-states. Therefore, any unrestricted representation of processes necessarily contains sophisticated formalisms to express dependencies between potentially infinite sets.

    Since one of our goals is to provide a robust and relatively concise model, we restrict the scope of abstract processes. For this purpose, we treat abstract processes as a meta-model of which we derive the ``Transit-Commit'' model (TC-model) that allows for restricted (and thus simpler) specification of processes. In section \ref{sc:concrete_model}, we formally specify the model as a subclass of abstract processes that can be expressed concisely, but still contain a meaningful set of processes. We provide a visual representation for these processes in section \ref{sc:concrete_visual} as well as a prototypical implementation in section \ref{sc:concrete_implementation}.


    \subsection{Specification of the Model}\label{sc:concrete_model}

    As a basic premise of the TC-model, we make the following assumptions:
    \begin{itemize}
        \item A set $E$ of agents that interact with processes. We impose no restrictions, $E$ could be either a particular set of users, such as the employees of a company, or a general set of terms describing arbitrary users.
        \item A set $R$ of roles and a relation $\mathsf{role}\subseteq E\times R$ to associate users with specific roles. The primary function of roles is to grant privileges to specific groups of users.
        \item We further assume, that all elements of sets discussed now and henceforth (such as $E$, $R$, $Q$, $K$, etc.) are finitary in the sense that finite, concrete terms can express their elements.
    \end{itemize}
    Based on these sets, we stipulate the notion of a transition.

    \begin{definition}\label{def:transition}
        Let $Q$ be a finite set (of states), let $K$ be a finite set (of keys), and let $M$ be a set of messages. A transition over $Q$ and $K$ is a tuple
        \begin{align*}
        (p,q,m,C_\mathsf{mand},C_\mathsf{opt}, C_\mathsf{role},C_\mathsf{data})
        \end{align*}
        where
        \begin{itemize}
            \item $p\in Q$ is the origin-state of the transition.
            \item $q\in Q$ is the exit-state of the transition.
            \item $m\in M$ is the message sent when the transition is executed.
            \item $C_\mathsf{mand}\subseteq K$ is the set of mandatory commit-data of the transition.
            \item $C_\mathsf{opt}\subseteq K$ is the set of optional commit-data of the transition.
            \item $C_\mathsf{role}\subseteq R$ is the finite set of roles authorized to initiate the transition.
            \item $C_\mathsf{data}\subseteq K$ describes which keys have to be associated with a value (in the array-state) before the transition can be initiated.
        \end{itemize}
    \end{definition}

    We are now able to derive a process from a (finite) family of transitions and an associative array as follows.

    \begin{definition}\label{def:TC-transitions}
        Let $T=(\tau_i)_{i\in I}$ be a finite family of transitions over $K$ and $Q$, and let $A$ be an associative array with keys in $K$. We define:
        \begin{align*}
            Q[T]&=\{(\tau_i)_1\mid i\in I\}\cup\{(\tau_i)_2\mid i\in I\}\\
            M[T]&=\{(\tau_i)_3\mid i\in I\}\\
            C[T,A]&=E\times I\times S_{A}\\
            \delta[T,A]&:Q\times S_A\times C_A\to Q\times S_A\times M
        \end{align*}
        with
        \begin{align*}
            \delta[T](p,s,(e,i,u))=(\check{p},\check{s},m)
        \end{align*}
        if and only if
        \begin{align*}
            \tau_i=&(p,\check{p},m,C_\mathsf{mand},C_\mathsf{opt}, C_\mathsf{role},C_\mathsf{data})\\
            &\land C_\mathsf{mand}\subseteq  \mathsf{keys}(u)\\
            &\land \mathsf{keys}(u)\subseteq C_\mathsf{mand}\cup C_\mathsf{opt}\\
            &\land \exists r\in C_\mathsf{role}(\mathsf{role}(e,r))\\
            &\land \check{s} = \mathsf{merge}(u,s)
        \end{align*}
        where $\mathsf{keys}(u)=\{k\mid\exists v((k,v)\in u) \}$. Now, given an initial state $q_{init}\in Q[T]$ and a set of final states $F\subseteq Q[T]$, we define the process
        \begin{align*}
            \llbracket T,A,q_{init},F \rrbracket=(Q[T], A,C[T,A], M[T],\delta[T,A],q_{init},F).
        \end{align*}
    \end{definition}

    \begin{remark}
    Since each commit-message (an element of $C[T]$) uniquely determines a transition, the function $\delta[T]$ is well defined.
    \end{remark}

    \begin{example}[Hardware Ordering]\label{proc:hwo}
        To illustrate the modeling of real-world business processes, we formalize the following informally described (simplified) process that describes the acquisition of new hardware by employees of a fictitious company $Corp$.

        \begin{enumerate}
            \item Employees can request new hardware by providing an offering (e.g., a link to an offering in a webshop)
            \item A procurement employee either approves or rejects the request
            \item If the request is rejected, then s reason for rejecting the proposal must be provided
        \end{enumerate}

        Before we start to model the ``hardware ordering'' process, we assume that $E$, $R$ and $\mathsf{role}$ are given as follows.
        \begin{itemize}
            \item $E$ is a set of identifiers, each of which uniquely identifies an employee of $Corp$.
            \item $R=\{\text{procurement},\text{hr},\text{sales},\dots\}$ is the set of roles in $Corp$.
            \item $\mathsf{role}(e,r)$ means that the employee (with identifier) $e$ works in role $r$.
        \end{itemize}

        To model the actual process, we first fix an associative array $A$ as follows:
        \begin{align*}
            \text{offer} &: \text{URL}\\
            \text{reason} &: \text{Text}
        \end{align*}
        Now we are ready to specify the individual transitions:
        \begin{align*}
            \tau_1 = &(\text{init}, \text{eval}     , \varepsilon               , \{\text{offer}\}  , \varnothing, \varnothing,\varnothing)\\
            \tau_2 = &(\text{eval}, \text{denied}   , \text{``Order denied''}   , \{\text{reason}\} , \varnothing, \{\text{procurement}\} , \varnothing)\\
            \tau_3 = &(\text{eval}, \text{accepted} , \text{``Order approved''} , \varnothing       , \varnothing, \{\text{procurement}\} , \varnothing)
        \end{align*}
        where $\varepsilon$ denotes the absence of a message. The final process is given from
        \begin{align*}
             \llbracket \{\tau_1,\tau_2,\tau_3\},A,\text{init},\{\text{denied},\text{accepted}\} \rrbracket.
        \end{align*}
        For illustration, the interaction sequence associated with the commit-messages
        \begin{align*}
            c_0 &= (\text{employeeX},1,\{(\text{offer}, \texttt{url\_to\_offer})\} )\\
            c_1 &= (\text{employeeY},2,\{(\text{reason}, \text{some\_reason})\} )
        \end{align*}
        is
        \begin{align*}
            \text{init},\varnothing&\xrightarrow{c_0 \varepsilon}
            \text{eval},\{(\text{offer},\texttt{url\_to\_offer})\}\\
            &\xrightarrow{c_1\: \text{``order denied''}} \text{denied},
            \left\{
                \begin{array}{r}
                    (\text{offer},\texttt{url\_to\_offer}),\\
                    (\text{reason},\text{some\_reason})
                \end{array}
            \right\}.
        \end{align*}

    \end{example}
    %    The specification leads to the following intended meaning of transitions given from a tuple $t=(p,q,C_\mathsf{mand},C_\mathsf{opt}, C_\mathsf{role},C_\mathsf{data})$:
    %    For an agent/employee $e$ to initiate a transition from state $p$ to state $q$, based on $t$, the following must be satisfied:
    %    \begin{itemize}
    %        \item The employee $e$ must work in a role listed in $C_\mathsf{role}$.
    %        \item $e$ must provide data that can be associated with key values as listed in $C_\mathsf{mand}$, e.g. think of ``mandatory fields'' in a form.
    %        \item $e$ is allowed to provide additional data, that will be associated with keys listed in $C_\mathsf{opt}$, e.g. think of ``optional filds'' in a form.
    %        \item The transition might only be executed if the array-state of the process is already filled to some defined degree.
    %    \end{itemize}

    %    -- | Description of the data needed to check whether the transition can
    %    -- be executed.
    %    type ValidationData
    %    = { -- | Roles that have permission to submit the action.
    %        checkRoles :: List RoleTag
    %        -- | Form fields that are required to go on.
    %        , requiredFields :: List FieldName
    %        -- | Additional optional form fields.
    %        , optionalFields :: List FieldName
    %        -- | Fields submitted in a previous step necessary to initiate the transition.
    %        , prerequiredFields :: List FieldName
    %    }
    %
    %    type Transition
    %    = { trOrigin :: State
    %        , trExit :: State
    %        , trRequest :: Request
    %        , trValidation :: ValidationData
    %    }


    \subsection{Visualization}\label{sc:concrete_visual}
    In the $TC$ model, a process is given from (a family %$T=(\tau_i)_{i\in I}$
    of) transitions and an associative array. In the following, we introduce modalities to represent and combine these components graphically.

    Associative arrays are represented as a list of associations, rendered in a box:
    \begin{align*}
        \boxed{
            \begin{array}{ccc}
                k_0 & : & A(k_0)\\
                k_1 & : & A(k_1)\\
                \vdots&  & \vdots
            \end{array}
        }
    \end{align*}

    States are depicted the same as when graphically representing finite state machines, as circular shapes with various annotations:
    \begin{align*}
        \includegraphics[width=0.6\textwidth]{images/simpleStates.pdf}
    \end{align*}
    The transitions are rendered as arrows starting at their origin-state and ending in the respective exit-state. The arrows are labelled with descriptions of the remaining contents of the transition: $m$, $C_\mathsf{mand}$, $C_\mathsf{opt}$, $C_\mathsf{role}$ and $C_\mathsf{data}$.
    \begin{align*}
        \includegraphics[scale=0.7]{images/simpleTransition.pdf}
    \end{align*}
    where ``msg'', ``mandatory'', ``optional'', ``role'' and ``data'' describe $m$, $C_\mathsf{mand}$, $C_\mathsf{opt}$, $C_\mathsf{role}$ and $C_\mathsf{data}$ respectively.

    Entire processes are composed of several transitions and the representation of a suitable associative array in one graph. For instance, the hardware ordering process as discussed in example \ref{proc:hwo} can be represented as follows:

    \begin{align*}
        \includegraphics[width=\textwidth]{images/proc1.pdf}
    \end{align*}




    \subsection{Implementation (Simulator)}\label{sc:concrete_implementation}

    We implemented a basic simulator of the $TC$-model with the idea to offer a simple method to explore how interacting with processes in our model works. The general idea in the simulator is to impersonate various agents and to communicate with processes through simple web-forms.

    The following functionalities of the simulator are currently available directly from within the user interface:
    \begin{itemize}
        \item Choosing a user/role to impersonate (as a user of the simulator).
        \item Executing transitions by completing and submitting web-forms (as the impersonated user).
        \item Observe state changes and interactions in a graphical representation of the process under evaluation.
    \end{itemize}
    Planned functionalities that currently are not available from within the user interface:
    \begin{itemize}
        \item Creating new users.
        \item Creating new processes.
        \item Creating and modifying web-forms.
    \end{itemize}
    The simulator can be accessed at \url{https://olodnad.gitlab.io/fsmprocesses/implementation/}

    Following is an annotated printout of code snippets that are most relevant to get a quick overview of the implementation. However, here we only discuss the data types and only those that are most particular to the $TC$-model. For a detailed documentation the reader is referred to the complete documentation (\url{https://olodnad.gitlab.io/fsmprocesses/documentation/}), or to the actual codebase (\url{https://gitlab.com/olodnad/fsmprocesses}).

    \input{implementation}

    \section{Conclusion and Outlook}
    In the present paper, we have specified an abstract model for (business) processes/agreements and a semantics to describe interactions that are possible under a given process.  The $TC$-model provides a concrete mode to specify a limited number of processes directly.  We have provided a simple prototypical simulator for the $TC$-Model.

    We plan to extend our system in the following three aspects:

    \begin{itemize}
        \item \textbf{$TC$-model extension:}  It is easy to see that the $TC$-model is very limited with respect to expressing dependencies of the state transition function from array-states. It is expressible that a variable assignment must include particular keys, but it is impossible to enforce properties on the values. For instance, in the hardware process, we cannot enforce that the user who accepts an order is different from the user that initiated the process. To express these kinds of dependencies, we will implement a subsystem to evaluate propositional formulas on variable assignments.
        \item \textbf{Process Automation:} Our artifacts define the shape of interactions that can take place between participating agents. The respective participants must provide the associated work (to generate commit-messages). In future extensions, we plan to add ``bots'', special users in the form of small programs that are encoded directly on the platform that hosts the processes. The bots will be able to interact with processes with predefined priviledges. This extension includes a scripting language to specify the behavior of bots.
        \item \textbf{Modular Processes:} Currently, transitions only exist within processes. We plan to add a new kind of transitions that connect states from different processes. This feature will allow for more modular process designs and expcicit integration of subprocesses.
        \item \textbf{Concurrency:} In the current system, every sequence of interactions takes place in a separate instance of a process. Executing process instances separately makes sense when there are no dependencies between parties that interact in different instances of a process. However, some processes require concurrency between users that would otherwise be operating in separate instances. Any process that involves the distribution of assets with limited supply has this property, for instance.  We plan to introduce a modified semantics that allows for multiple groups of users to be active in one process concurrently. The basic idea is to allow multiple states to be active at the same time while maintaining a (partially) shared global array-state.
    \end{itemize}


    % ---- Bibliography ----

    \bibliographystyle{splncs04}

    \bibliography{bib}



\end{document}
