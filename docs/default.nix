{ sources ? import ./nix/sources.nix
, pkgs ? import ./nix { inherit sources; }
}:
let
    fontsConf = pkgs.makeFontsConf {
        fontDirectories = [
            pkgs.fira-code
            pkgs.tex-gyre.pagella
            pkgs.tex-gyre-math.pagella
            pkgs.lmodern
            pkgs.font-awesome_4
            pkgs.lato
        ];
    };
in
    pkgs.stdenv.mkDerivation rec {
        name = "process-doc";
        env = pkgs.buildEnv { name = name; paths = buildInputs; };
        src = pkgs.nix-gitignore.gitignoreSourcePure [ ./.gitignore ] ./.;
        buildInputs = [
            pkgs.which
            pkgs.fontconfig
            pkgs.fd
            pkgs.graphviz
            (pkgs.texlive.combine {
                inherit (pkgs.texlive)
                    scheme-small
                    classicthesis
                    fontawesome
                    tcolorbox
                    environ
                    trimspaces
                    enumitem
                    dashrule
                    ifmtarg
                    multirow
                    changepage
                    biblatex
                    logreq
                    stmaryrd
                    url
                    xstring;
            })
        ];
        FONTCONFIG_FILE = fontsConf;
        phases = [
            "unpackPhase"
            "buildPhase"
            "installPhase"
            "distPhase"
        ];

        buildPhase = ''
            make build-full
        '';

        installPhase = ''
            mkdir -p $out
            cp -r process.pdf $out
        '';
    }
