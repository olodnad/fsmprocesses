-- | A high-level description of forms.
module Processes.Form.Types where

import Processes.AppPrelude
import Data.Date (Date)
import Data.List (List, fromFoldable)

data BasicValue
  = TDate Int Int Int
  | TText String
  | TNumber Number
  | TBool Boolean

instance showBasicValue :: Show BasicValue where
  show (TDate year month day) = "TDate show not implemented"
  show (TText t) = t
  show (TNumber t) = show t
  show (TBool t) = if t then "Ja" else "Nein"

-- | An input needs a human readable label.
type Label
  = String

-- | The placeholder that should be displayed in input boxes.
type PlaceholderText
  = String

type UnitOfMeasurement
  = String

-- | A value with a label.
type LabeledValue
  = { label :: Label
    , value :: BasicValue
    }

-- | The dynamic parts of a form
-- | where user input is expected.
data FormEntryDefinitionDynamic
  = MultiSelectDefinition (List LabeledValue)
  | RadioDefinition (List LabeledValue)
  | LineDefinition PlaceholderText
  | MultiLineDefinition PlaceholderText
  | DateDefinition
  | UnitNumberDefinition UnitOfMeasurement

-- | The fixed parts of a form, like titles and explanation paragraphs.
data FormEntryDefinitionFixed
  = Title String
  | Paragraph String

-- | A `FormEntryDefinition` can be either `Dynamic` or `Fixed`.
-- | The `Dynamic` variant needs a `FieldName` under wich the
-- | inserted data is saved and a human readable label.
data FormEntryDefinition
  = Dynamic FieldName Label FormEntryDefinitionDynamic
  | Fixed FormEntryDefinitionFixed

-- | A full form is a sequence of `FormEntryDefinition`s.
newtype FormDefinition
  = FormDefinition { entries :: List FormEntryDefinition }

unFormDefinition :: FormDefinition -> { entries :: List FormEntryDefinition }
unFormDefinition (FormDefinition formDef) = formDef

-- | The 'FormEntry' holds the user inserted values.
data FormEntry
  = MultiSelect (List BasicValue)
  | Radio BasicValue
  | Line String
  | MultiLine String
  | Date Date
  | Number Number

-- | The key in the association where the value will be
-- | saved under.
type FieldName
  = String

-- | A line of a form, with the filled out data.
-- note: the hardcoded type FormData forbids us to write a function
-- to optimize form structure based on properties. could be changed to abstract type
type NamedFormEntry
  = { fieldName :: FieldName
    , entry :: FormEntry
    }

derive instance eqFormEntry :: Eq FormEntry

derive instance ordFormEntry :: Ord FormEntry

derive instance eqBasicValue :: Eq BasicValue

derive instance ordBasicValue :: Ord BasicValue

-- | A list of fieldNames and their entry. An entry can
-- | be thought of as a line of a form.
data FormData
  = FormData { fields :: List NamedFormEntry }

unFormData :: FormData -> { fields :: List NamedFormEntry }
unFormData (FormData formData) = formData

emptyFormData :: FormData
emptyFormData = FormData { fields: fromFoldable [] }
