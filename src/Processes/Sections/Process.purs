module Sections.Process where

import Processes.AppPrelude
import Data.Either (Either(..))
import Data.List (List, toUnfoldable)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Symbol (SProxy(..))
import Data.Tuple (Tuple(..))
import Effect.Aff (Aff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Processes.Form.Render as Render
import Processes.Form.Types (FormData, emptyFormData)
import Processes.Model.Types (Error, Request, TransitionId, Snapshot, User, Process)
import Processes.Model.UpdateSystem (scan, getRequest, updateCommit)
import Processes.Samples.Hardware (defaultUser)
import Processes.Sections.Process.Snapshot as SnapshotView
import Processes.UI (mkClass)

type Slot
  = H.Slot Query ProcessState

data Query a
  = GetProcess (ProcessState -> a)

data Action
  = UpdateProcessState ProcessState
  | SubmitFormData User TransitionId FormData
  | SetUser User
  | SetSelectedTransition TransitionId

type ProcessState
  = { formState :: FormData
    , snapshot :: Snapshot
    , error :: Maybe Error
    , currentUser :: User
    , users :: List User
    , name :: String
    , mSelectedTransitionId :: Maybe TransitionId
    }

component :: H.Component HH.HTML Query Process ProcessState Aff
component =
  H.mkComponent
    { initialState: initialState
    , render: render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            , handleQuery = handleQuery
            }
    }

initialState :: Process -> ProcessState
initialState process =
  { formState: emptyFormData
  , snapshot: process.snapshot
  , error: Nothing
  , currentUser: defaultUser
  , users: process.users
  , name: process.name
  , mSelectedTransitionId: Nothing
  }

type ChildSlots
  = ( form :: Render.Slot TransitionId
    , snapshot :: SnapshotView.Slot String
    )

render :: ProcessState -> H.ComponentHTML Action ChildSlots Aff
render state =
  HH.div
    [ mkClass "row" ]
    [ HH.div
        [ mkClass "card col-12" ]
        [ HH.h4
            [ mkClass "card-header" ]
            [ HH.text state.name ]
        , HH.div
            [ mkClass "card-body" ]
            [ HH.div [ mkClass "row" ]
                [ HH.div
                    [ mkClass "col-md-3" ]
                    [ HH.h5
                        []
                        [ HH.text "Please choose which form to answer next" ]
                    , HH.ul
                        []
                        ( toUnfoldable $ map (\fieldName -> HH.li [ HE.onClick $ (\_ -> Just $ SetSelectedTransition fieldName) ] [ HH.text fieldName ]) scanned
                        )
                    , HH.hr_
                    , HH.h5
                        []
                        [ HH.text "Users" ]
                    , HH.div
                        [ mkClass "list-group" ]
                        ( toUnfoldable
                            $ map
                                ( \user ->
                                    HH.a
                                      [ mkClass (if user == state.currentUser then "list-group-item list-group-item-action active" else "list-group-item list-group-item-action")
                                      , HE.onClick $ (\_ -> Just $ SetUser user)
                                      ]
                                      [ HH.text user.userId
                                      , HH.ul
                                          []
                                          (toUnfoldable $ map (\role -> HH.li_ [ HH.text role ]) user.userRoles)
                                      ]
                                )
                                state.users
                        )
                    ]
                , HH.div
                    [ mkClass "col-md-9" ]
                    [ case state.error of
                        Just err -> HH.div [ mkClass "alert alert-danger" ] [ HH.text err ]
                        Nothing -> HH.span_ []
                    , case mSelected of
                        Right (Tuple transitionId request) ->
                          HH.div
                            []
                            [ HH.h5
                                []
                                [ HH.text "Current Request" ]
                            , HH.slot
                                _form
                                transitionId
                                Render.component
                                { formData: state.formState, formDefinition: request.formDefinition }
                                ( \renderState ->
                                    state { formState = renderState.formData }
                                      |> UpdateProcessState
                                      |> Just
                                )
                            , HH.button
                                [ mkClass "btn btn-primary"
                                , HE.onClick $ (\_ -> Just $ SubmitFormData state.currentUser transitionId state.formState)
                                ]
                                [ HH.text "submit" ]
                            ]
                        Left err -> HH.div [ mkClass "alert alert-info" ] [ HH.text err ]
                    ]
                ]
            ]
        ]
    , HH.div [ mkClass "card col-12" ]
        [ HH.h4
            [ mkClass "card-header" ]
            [ HH.text "Snapshot" ]
        , HH.div
            [ mkClass "card-body" ]
            [ HH.slot
                _snapshot
                (fromMaybe "default-snapshot-svg" state.mSelectedTransitionId)
                SnapshotView.snapshotComponent
                { snapshot: state.snapshot }
                ( \svgstate ->
                    Nothing
                )
            ]
        ]
    ]
  where
  scanned :: List TransitionId
  scanned = scan state.snapshot state.currentUser

  -- do not show first form, but show a selection which form wants to be answered
  mSelected :: Either Error (Tuple TransitionId Request)
  mSelected = case state.mSelectedTransitionId of
    Just tId -> case getRequest tId state.snapshot of
      Right request -> Right $ Tuple tId request
      Left err -> Left err
    Nothing -> Left ("You are all done for now." :: Error)

_form = SProxy :: SProxy "form"

_snapshot = SProxy :: SProxy "snapshot"

handleAction :: forall a b c. Action -> H.HalogenM ProcessState c b ProcessState a Unit
handleAction = case _ of
  (UpdateProcessState process) -> do
    H.put process
    currentState <- H.get
    H.raise currentState
  (SetUser user) -> do
    H.modify_
      $ _
          { currentUser = user
          , mSelectedTransitionId = Nothing
          }
  (SetSelectedTransition transitionId) -> do
    H.modify_ $ _ { mSelectedTransitionId = Just transitionId }
  (SubmitFormData user transitionId formdata) -> do
    let
      commit = { sender: user, transition: transitionId, formData: formdata }
    currentState <- H.get
    case updateCommit commit currentState.snapshot of
      Right updatedSnapshot -> do
        H.modify_
          $ _
              { snapshot = updatedSnapshot
              , error = Nothing
              , formState = emptyFormData
              , mSelectedTransitionId = Nothing
              }
        cState <- H.get
        H.raise cState
      Left err -> H.modify_ $ _ { error = Just err }

handleQuery ::
  forall o m a t.
  Query a ->
  H.HalogenM ProcessState Action t
    o
    m
    (Maybe a)
handleQuery = case _ of
  GetProcess process -> Just <<< process <$> H.get
