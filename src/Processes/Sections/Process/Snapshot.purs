module Processes.Sections.Process.Snapshot where

import Processes.AppPrelude
import Data.List (List, toUnfoldable)
import Data.Map as MS
import Data.Maybe (Maybe(..))
import Data.Set as Set
import Data.Tuple (Tuple(..))
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff)
import Processes.Form.Render (entryToText)
import Processes.Form.Types (FieldName, FormEntry)
import Processes.Graphviz (renderToSvg, snapshotToGraph)
import Halogen as H
import Halogen.HTML (HTML, IProp)
import Halogen.HTML as HH
import Processes.Model.Types (Snapshot)
import Svg.Renderer.Halogen (icon)
import Processes.UI (mkClass)

type Slot
  = H.Slot Query State

data Action
  = Initialize

type State
  = { svg :: String, snapshot :: Snapshot }

data Query a
  = GetState (State -> a)

snapshotComponent :: H.Component HH.HTML Query { snapshot :: Snapshot } State Aff
snapshotComponent =
  H.mkComponent
    { initialState
    , render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            , initialize = Just Initialize
            , finalize = Nothing
            }
    }
  where
  initialState :: { snapshot :: Snapshot } -> State
  initialState i = { svg: "", snapshot: i.snapshot }

render :: forall a b. State -> HH.HTML a b
render state =
  HH.div
    []
    [ HH.div
        [ mkClass "row" ]
        [ HH.div
            [ mkClass "col-md-4 col-xs-12" ]
            [ HH.h5_ [ HH.text "Current State" ]
            , HH.p [] [ HH.text snapshot.currentState ]
            ]
        , HH.div
            [ mkClass "col-md-4 col-xs-12" ]
            [ HH.h5_ [ HH.text "Transitions" ]
            , HH.p
                []
                [ HH.ul_
                    ( map
                        ( \transition ->
                            HH.li_
                              [ HH.text transition ]
                        )
                        ( Set.toUnfoldable
                            $ MS.keys
                                snapshot.transitions
                        )
                    )
                ]
            ]
        , HH.div
            [ mkClass "col-md-4 col-xs-12" ]
            [ HH.h5_ [ HH.text "Data" ]
            , HH.ul
                [ mkClass "list-group" ]
                ( toUnfoldable
                    $ map
                        ( \(Tuple (fieldName :: FieldName) (formEntries :: List FormEntry)) ->
                            HH.div
                              [ mkClass "list-group-item" ]
                              [ HH.text fieldName
                              , HH.ul_
                                  ( toUnfoldable
                                      $ map
                                          ( \(entry :: FormEntry) ->
                                              HH.li_ [ HH.text $ entryToText entry ]
                                          )
                                          formEntries
                                  )
                              ]
                        )
                        snapshotTuples
                )
            ]
        ]
    , HH.div
        [ mkClass "row" ]
        [ svg [] ]
    ]
  where
  snapshotTuples :: List (Tuple FieldName (List FormEntry))
  snapshotTuples = MS.toUnfoldable snapshot.data

  snapshot :: Snapshot
  snapshot = state.snapshot

  svg :: Icon
  svg = icon state.svg

type Icon
  = forall p r i. Array (IProp r i) -> HTML p i

handleAction :: forall a b c. MonadAff a => Action -> H.HalogenM State c b State a Unit
handleAction = case _ of
  Initialize -> do
    currentState <- H.get
    let
      str = snapshotToGraph currentState.snapshot
    r <- H.liftAff (renderToSvg str)
    H.modify_ (_ { svg = r })

handleQuery :: forall p o m a. Query a -> H.HalogenM State Action p o m (Maybe a)
handleQuery = case _ of
  GetState state -> Just <<< state <$> H.get
