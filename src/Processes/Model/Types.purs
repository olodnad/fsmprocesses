module Processes.Model.Types where

import Data.List (List)
import Data.Map as MS
import Processes.Form.Types (FormDefinition, FormData, FormEntry)

-- | The name of a form field.
type FieldName
  = String

-- | Typed global variable assignment with history.
type Data
  = MS.Map FieldName (List FormEntry)

-- | State of the finite state machine.
type State
  = String

-- | Name of a process
type ProcessName
  = String

-- | A unique name for each state transition in the finite state machine.
type TransitionId
  = String

-- | A role is a tag which can be assigned to a user for permission purposes.
type RoleTag
  = String

-- | A unique name for a user interacting with a process.
type UserId
  = String

-- | A user with roles
type User
  = { userId :: UserId
    , userRoles :: List RoleTag
    }

-- | User submitted form, the form data provided by a user when attempting to execute a transition.
-- | for example a User has filled out a form X
-- | - `sender`:
-- | - `formData`
type Commit
  = { sender :: User
    , transition :: TransitionId
    , formData :: FormData
    }

-- | All stuff the user needs to execute a given transition.
-- |
-- | `formDefinition` The form definition to render
-- | Think: User Dandolo must fill out fields name and address, and can optionally provide his birthday
-- Todo: Generated from 'requiredFields' and 'optionalFields' in ValidationData
-- `rRequired` Fields that need to be submitted.
-- `rOptional` Optional additional fields.
-- when a Request is being built to send to the user.
type Request
  = { formDefinition :: FormDefinition
    } -- rRequired :: List FieldName -- , rOptional :: List FieldName

type Error
  = String

-- | Description of the data needed to check whether the transition can
-- | be executed.
-- |
-- | - `checkRoles` Roles that have permission to submit the action.
-- | - `requiredFields` Form fields that are required to go on.
-- | - `optionalFields` Additional optional form fields.
-- | - `prerequiredFields` Fields submitted in a previous step necessary to initiate the transition.
type ValidationData
  = { checkRoles :: List RoleTag
    , requiredFields :: List FieldName
    , optionalFields :: List FieldName
    , prerequiredFields :: List FieldName
    }

-- | A transition has an `origin` and an `exit`. It describes something
-- | like `A -> B`, where the the arrow holds the `request` and `validation`
type Transition
  = { origin :: State
    , exit :: State
    , request :: Request
    , validation :: ValidationData
    }

type Transitions
  = MS.Map TransitionId Transition

-- | Describes a momentary state of the process.
-- | - `currentState` holds the state of the finite state machine.
-- | - `transitions` holds all possible transitions of the state machine.
-- | - `data` Global variable assignment with history.
type Snapshot
  = { currentState :: State
    , transitions :: Transitions
    , data :: Data
    }

-- | All Data regarding a process.
-- |
-- | - `snapshot` is the current state of the process.
-- | - `name` is the name of the process.
-- | - `users` are a list of participants in this process.
type Process
  = { snapshot :: Snapshot
    , name :: ProcessName
    , users :: List User
    }
