module Processes.Model.UpdateSystem where

import Processes.AppPrelude (bind, discard, map, not, pure, when, (#), ($), (&&), (<$>), (<>), (==))
import Data.Either (Either(..))
import Data.List (List, null, singleton, union)
import Data.Maybe (Maybe(..))
import Processes.Model.Types
  ( Commit
  , Data
  , Error
  , FieldName
  , Request
  , RoleTag
  , Snapshot
  , Transition
  , TransitionId
  , Transitions
  , User
  , UserId
  )
import Data.List.Extra (disjoint, isSubsequenceOf)
import Data.Map as MS
import Data.Set as Set
import Data.Tuple (Tuple(..))
import Processes.Form.Types (FormData(..), FormEntry, NamedFormEntry, unFormData)

-- | Get currently available transactions.
scan :: Snapshot -> User -> List TransitionId
scan snapshot user = ks filtered
  where
  transitions :: Transitions
  transitions = snapshot.transitions

  filtered :: MS.Map TransitionId Transition
  filtered =
    MS.filter
      ( \(transition :: Transition) ->
          snapshot.currentState == transition.origin
            && not (transition.validation.checkRoles `disjoint` user.userRoles)
      )
      transitions

  ks :: MS.Map TransitionId Transition -> List TransitionId
  ks s = MS.keys s # Set.toUnfoldable

-- | Get information about required input for a given transaction.
getRequest :: TransitionId -> Snapshot -> Either Error Request
getRequest trId sn = case tr of
  Nothing -> Left $ "Transition not found"
  Just transition -> Right transition.request
  where
  tr = MS.lookup trId $ sn.transitions

-- | Verify if a commit is suitable to execute a given transition
-- | * check if all necessary input is provided
-- | * check if commiting user has right to execute transition
-- | if verification is succesfull, return new snapshot, otherwise
-- | return error message
updateCommit :: Commit -> Snapshot -> Either Error Snapshot
updateCommit c sn = do
  valData <- _.validation <$> tr
  when
    (valData.checkRoles `disjoint` senderRoles)
    (Left "Transition denied: user access denied")
  let
    admissibleCommitData = valData.requiredFields <> valData.optionalFields
  when
    ( not (valData.requiredFields `isSubsequenceOf` inputFields)
    )
    (Left $ "Transition denied: data missing in commit")
  when
    (not (inputFields `isSubsequenceOf` admissibleCommitData))
    (Left "Transition denied: excess data in commit")
  when
    (not (valData.prerequiredFields `isSubsequenceOf` stateData))
    (Left "Transition denied: data missing in state")
  newState <- map _.exit tr
  pure
    $ sn
        { currentState = newState
        , data = updateData input sn.data
        }
  where
  senderId :: UserId
  senderId = c.sender.userId

  senderRoles :: List RoleTag
  senderRoles = c.sender.userRoles

  input :: FormData
  input = c.formData

  inputFields :: List FieldName
  inputFields = map _.fieldName (unFormData input).fields

  currentState :: _
  currentState = sn.currentState

  tr :: _
  tr = case MS.lookup c.transition sn.transitions of
    Nothing -> Left $ "Transition not found"
    Just tr' -> Right $ tr'

  stateData :: _
  stateData =
    Set.toUnfoldable $ MS.keys
      $ MS.filter (\d -> not (null d))
      $ sn.data

  -- | Takes the existing data and adds the new data.
  updateData :: FormData -> Data -> Data
  updateData fData prevData = mergeData prevData (newData fData)
    where
    newData :: FormData -> MS.Map FieldName (List FormEntry)
    newData (FormData formData) =
      MS.fromFoldable
        $ map
            (\(namedFormEntry :: NamedFormEntry) -> Tuple namedFormEntry.fieldName (singleton namedFormEntry.entry))
            formData.fields

    -- mergeData :: Map a [b] -> Map a b' -> Map a [b':b]
    -- intersectionWith :: forall k a b c. Ord k => (a -> b -> c) -> Map k a -> Map k b -> Map k c
    mergeData :: Data -> MS.Map FieldName (List FormEntry) -> Data
    mergeData = MS.unionWith (\a b -> union a b)
