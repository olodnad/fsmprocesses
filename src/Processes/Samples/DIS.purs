module  Processes.Samples.DIS where

import Processes.AppPrelude (($))
import Processes.Model.Types (Snapshot, Transition, Transitions, User, Process)
import Data.List (fromFoldable)
import Processes.Form.Types
  ( BasicValue(..)
  , FormDefinition(..)
  , FormEntryDefinition(..)
  , FormEntryDefinitionDynamic(..)
  , FormEntryDefinitionFixed(..)
  )
import Data.Map as MS
import Data.Tuple (Tuple(..))

process :: Process
process =
  { snapshot: initialSnapshot
  , name: "DIS"
  , users: fromFoldable [ defaultUser, bankUser, disProvider ]
  }

defaultUser :: User
defaultUser =
  { userId: "customer"
  , userRoles: fromFoldable [ "customer" ]
  }

bankUser :: User
bankUser =
  { userId: "bank"
  , userRoles: fromFoldable [ "bank" ]
  }

disProvider :: User
disProvider =
  { userId: "disProvider"
  , userRoles: fromFoldable [ "disProvider" ]
  }

initialSnapshot :: Snapshot
initialSnapshot =
  { currentState: "init"
  , transitions: transitions
  , data:
    MS.fromFoldable
      []
  }

transitions :: Transitions
transitions =
  MS.fromFoldable
    [ Tuple "sendAccountInfo" sendAccountInfo
    , Tuple "confirmAccountInfo" confirmAccountInfo
    , Tuple "checkIdentity" checkIdentity
    , Tuple "bankStatus" bankStatus
    , Tuple "abort" abort
    ]

sendAccountInfo :: Transition
sendAccountInfo =
  { origin: "init"
  , exit: "sentAccountInfo"
  , request:
    { formDefinition: onboardForm
    }
  , validation:
    { checkRoles: fromFoldable [ "customer" ]
    , requiredFields: fromFoldable [ "firstname", "lastname", "birthdate", "address" ]
    , optionalFields: fromFoldable [ "initialDeposit" ]
    , prerequiredFields: fromFoldable []
    }
  }

confirmAccountInfo :: Transition
confirmAccountInfo =
  { origin: "sentAccountInfo"
  , exit: "confirmedAccountInfo"
  , request:
    { formDefinition: confirmAccountInfoForm
    }
  , validation:
    { checkRoles: fromFoldable [ "bank" ]
    , requiredFields: fromFoldable []
    , optionalFields: fromFoldable [ "signature" ]
    , prerequiredFields: fromFoldable []
    }
  }

abort :: Transition
abort =
  { origin: "confirmedAccountInfo"
  , exit: "exit"
  , request:
    { formDefinition: abortForm
    }
  , validation:
    { checkRoles: fromFoldable [ "bank", "user", "disProvider" ]
    , requiredFields: fromFoldable []
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

checkIdentity :: Transition
checkIdentity =
  { origin: "confirmedAccountInfo"
  , exit: "checkedIdentity"
  , request:
    { formDefinition: disForm
    }
  , validation:
    { checkRoles: fromFoldable [ "disProvider" ]
    , requiredFields: fromFoldable [ "approved" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable [ "signature" ]
    }
  }

bankStatus :: Transition
bankStatus =
  { origin: "checkedIdentity"
  , exit: "done"
  , request:
    { formDefinition: bankStatusForm
    }
  , validation:
    { checkRoles: fromFoldable [ "bank" ]
    , requiredFields: fromFoldable []
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

bankStatusForm :: FormDefinition
bankStatusForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "DIS Process is done. ")
        , Fixed (Title "please check the collected data and proceed with informing the customer")
        ]
    }

abortForm :: FormDefinition
abortForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "This process has not been approved. Sorry about that, now please move along. ")
        ]
    }

confirmAccountInfoForm :: FormDefinition
confirmAccountInfoForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "A new customer is onboarding, please check the data.")
        , Fixed (Paragraph "Please sign if you want to allow the customer to authenticate, otherwise continue without signature.")
        , Dynamic "signature" "Signature" (LineDefinition "Optional: Signature (sign if process should be continued)")
        ]
    }

disForm :: FormDefinition
disForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Please check the customer on the video")
        , Fixed (Paragraph "The user must look straigt into the camera with a picture of the passport. Then the user must hold the camera to show the left and the right side of the face.")
        , Dynamic "approved" "Have you checked all the requirements? Are they fullfilled?"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "yes"
                      , value: TText "yes"
                      }
                    , { label: "no"
                      , value: TText "no"
                      }
                    ]
            )
        ]
    }

onboardForm :: FormDefinition
onboardForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Open a new account")
        , Dynamic "firstname" "First Name" (LineDefinition "Hans")
        , Dynamic "lastname" "Last Name" (LineDefinition "Meier")
        , Dynamic "birthdate" "Birthdate" (LineDefinition "15.06.1991")
        , Dynamic "address" "Address" (LineDefinition "Zürcherstrasse 1, 8400 Winterthur")
        , Dynamic "initialDeposit" "Initial Deposit" (UnitNumberDefinition "0.0")
        ]
    }
