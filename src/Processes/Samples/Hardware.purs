module  Processes.Samples.Hardware where

import Processes.AppPrelude (($))
import Processes.Model.Types (Snapshot, Transition, Transitions, User, Process)
import Data.List (fromFoldable)
import Processes.Form.Types (BasicValue(..), FormDefinition(..), FormEntryDefinition(..), FormEntryDefinitionDynamic(..), FormEntryDefinitionFixed(..))
import Data.Map as MS
import Data.Tuple (Tuple(..))

process :: Process
process =
  { snapshot: initialSnapshot
  , name: "Hardware Process"
  , users: fromFoldable [ defaultUser, bossUser ]
  }

defaultUser :: User
defaultUser =
  { userId: "dandolo"
  , userRoles: fromFoldable [ "employee" ]
  }

bossUser :: User
bossUser =
  { userId: "joris"
  , userRoles: fromFoldable [ "boss" ]
  }

initialSnapshot :: Snapshot
initialSnapshot =
  { currentState: "root"
  , transitions: transitions
  , data:
    MS.fromFoldable
      [ Tuple "hardwareList" (fromFoldable [])
      , Tuple "confirmation" (fromFoldable [])
      ]
  }

transitions :: Transitions
transitions =
  MS.fromFoldable
    [ Tuple "chooseHardware" root
    , Tuple "reviewHardware" reviewHardware
    ]

root :: Transition
root =
  { origin: "root"
  , exit: "hardwareChosen"
  , request:
    { formDefinition: requestForm
    }
  , validation:
    { checkRoles: fromFoldable [ "employee" ]
    , requiredFields: fromFoldable [ "hardwareList" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

reviewHardware :: Transition
reviewHardware =
  { origin: "hardwareChosen"
  , exit: "exit"
  , request:
    { formDefinition: isThisOkayForm
    }
  , validation:
    { checkRoles: fromFoldable [ "boss" ]
    , requiredFields: fromFoldable [ "confirmation" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

isThisOkayForm :: FormDefinition
isThisOkayForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Please confirm this order")
        , Dynamic "confirmation" "Is this okay?"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "yes"
                      , value: TText "yes"
                      }
                    , { label: "no"
                      , value: TText "no"
                      }
                    ]
            )
        ]
    }

requestForm :: FormDefinition
requestForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Hi, please submit this form. ")
        , Dynamic "hardwareList" "Hardware"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "Lenovo"
                      , value: TText "lenovo"
                      }
                    , { label: "Huawei"
                      , value: TText "huawei"
                      }
                    , { label: "Microsoft"
                      , value: TText "microsoft"
                      }
                    ]
            )
        ]
    }
