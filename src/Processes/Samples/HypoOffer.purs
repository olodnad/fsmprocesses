module  Processes.Samples.HypoOffer where

import Processes.AppPrelude (($), (|>))
import Data.List (fromFoldable, (:), concat)
import Data.Map as MS
import Data.Tuple (Tuple(..))
import Processes.Form.Types
  ( BasicValue(..)
  , FormDefinition(..)
  , FormEntryDefinition(..)
  , FormEntryDefinitionDynamic(..)
  , FormEntryDefinitionFixed(..)
  )
import Processes.Model.Types (Snapshot, Transition, Transitions, User, Process)

process :: Process
process =
  { snapshot: initialSnapshot
  , name: "Hypo Offer Process"
  , users: fromFoldable [ customer, bank ]
  }

customer :: User
customer =
  { userId: "Mr Mueller"
  , userRoles: fromFoldable [ "customer" ]
  }

bank :: User
bank =
  { userId: "Mr"
  , userRoles: fromFoldable [ "bank" ]
  }

initialSnapshot :: Snapshot
initialSnapshot =
  { currentState: "root"
  , transitions: transitions
  , data: MS.fromFoldable []
  }

transitions :: Transitions
transitions =
  MS.fromFoldable
    [ Tuple "initialRequest" initialRequestTransition
    , Tuple "requestOffer" requestOffer
    , Tuple "resignOffer" resignOfferTransition
    , Tuple "amendRequest" amendRequestTransition
    , Tuple "amendOffer" ammendOfferTransition
    , Tuple "commentOffer" commentOfferTransition
    , Tuple "bankResigns" bankResignsTransition
    , Tuple "bankAccepts" bankAcceptsOfferTransition
    , Tuple "customerAccepts" customerAcceptsOfferTransition
    ]

initialRequestTransition :: Transition
initialRequestTransition =
  { origin: "root"
  , exit: "request"
  , request:
    { formDefinition: requestForm
    }
  , validation: amendRequestTransition.validation
  }

requestOffer :: Transition
requestOffer =
  { origin: "request"
  , exit: "offer"
  , request:
    { formDefinition: requestOfferForm
    }
  , validation:
    { checkRoles: fromFoldable [ "customer" ]
    , requiredFields: fromFoldable []
    , optionalFields: fromFoldable []
    , prerequiredFields: amendRequestTransition.validation.requiredFields
    }
  }

ammendOfferTransition :: Transition
ammendOfferTransition =
  { origin: "offer"
  , exit: "offer"
  , request:
    { formDefinition: offerForm
    }
  , validation:
    { checkRoles: fromFoldable [ "customer", "bank" ]
    , requiredFields: fromFoldable [ "offer" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

offerForm :: FormDefinition
offerForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Offer Form")
        , Dynamic "offer" "Offer" (LineDefinition "i.e. Unser Angebot: Zu Zins xy Laufzeit z. ")
        ]
    }

commentOfferTransition :: Transition
commentOfferTransition =
  { origin: "offer"
  , exit: "offer"
  , request:
    { formDefinition: commentOfferForm
    }
  , validation:
    { checkRoles: fromFoldable [ "customer", "bank" ]
    , requiredFields: fromFoldable [ "offerComment" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

commentOfferForm :: FormDefinition
commentOfferForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Offer Form")
        , Dynamic "offerComment" "Kommentar zur Offerte" (LineDefinition "i.e. Zins ist zu hoch")
        ]
    }

amendRequestTransition :: Transition
amendRequestTransition =
  { origin: "request"
  , exit: "request"
  , request:
    { formDefinition:
      requestForm
        |> ( \(FormDefinition form) ->
              FormDefinition
                { entries:
                  ( commentField : form.entries
                  )
                }
          )
    }
  , validation:
    { checkRoles: fromFoldable [ "customer" ]
    , requiredFields: fromFoldable [ "income", "ownCapital", "runtime", "utilization", "objectType" ]
    , optionalFields: concat $ fromFoldable [ fromFoldable [ "financingProfile", "buyingPrice" ], fromFoldable [ "requestComment" ] ]
    , prerequiredFields: fromFoldable []
    }
  }
  where
  commentField :: FormEntryDefinition
  commentField = (Dynamic "requestComment" "Kommentar" (LineDefinition ""))

customerAcceptsOfferTransition :: Transition
customerAcceptsOfferTransition =
  { origin: "offer"
  , exit: "inNegotiation"
  , request:
    { formDefinition: inNegotiationForm
    }
  , validation:
    { checkRoles: fromFoldable [ "customer" ]
    , requiredFields: fromFoldable [ "customerSignature" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

inNegotiationForm :: FormDefinition
inNegotiationForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Ich akzeptiere die Bedingungen")
        , Dynamic "customerSignature" "Unterschrift Kunde" (LineDefinition "")
        ]
    }

bankAcceptsOfferTransition :: Transition
bankAcceptsOfferTransition =
  { origin: "inNegotiation"
  , exit: "bankAccepted"
  , request:
    { formDefinition:
      FormDefinition
        { entries:
          fromFoldable
            [ Fixed (Title "Wir sagen zu")
            , Dynamic "bankSignature" "Unterschrift Bankvertretung" (LineDefinition "")
            ]
        }
    }
  , validation:
    { checkRoles: fromFoldable [ "bank" ]
    , requiredFields: fromFoldable [ "bankSignature" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

bankResignsTransition :: Transition
bankResignsTransition =
  { origin: "inNegotiation"
  , exit: "resignedBank"
  , request:
    { formDefinition:
      FormDefinition
        { entries:
          fromFoldable
            [ Fixed (Title "Wir lehnen die Anfrage ab")
            ]
        }
    }
  , validation:
    { checkRoles: fromFoldable [ "bank" ]
    , requiredFields: fromFoldable []
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

resignOfferTransition :: Transition
resignOfferTransition =
  { origin: "offer"
  , exit: "resignedCustomer"
  , request:
    { formDefinition: resignOfferForm
    }
  , validation:
    { checkRoles: fromFoldable [ "customer" ]
    , requiredFields: fromFoldable [ "offerResignedReason" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

resignOfferForm :: FormDefinition
resignOfferForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Ablehnen")
        , Dynamic "offerResignedReason" "Grund für die Ablehnung" (LineDefinition "")
        ]
    }

requestOfferForm :: FormDefinition
requestOfferForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Anfrage senden")
        ]
    }

requestForm :: FormDefinition
requestForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Anfrage")
        , Dynamic "income" "Einkommen p.a. brutto *" (UnitNumberDefinition "0.0")
        , Dynamic "buyingPrice" "Entweder der angestrebte Kaufpreis oder offen, wenn noch kein Preis bekannt ist. *" (UnitNumberDefinition "0.0")
        , Dynamic "ownCapital" "Eigenmittel *" (UnitNumberDefinition "0.0")
        , Dynamic "runtime" "Laufzeit *" (UnitNumberDefinition "5.0")
        , Dynamic "utilization" "Utilization *"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "Rendite"
                      , value: TText "yield"
                      }
                    , { label: "Ferien"
                      , value: TText "vacation"
                      }
                    , { label: "Eigenheim"
                      , value: TText "inhabit"
                      }
                    ]
            )
        , Dynamic "objectType" "Objekt Typ"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "Grundstück"
                      , value: TText "lot"
                      }
                    , { label: "Wohnung"
                      , value: TText "appartment"
                      }
                    , { label: "Reihenhaus"
                      , value: TText "townhouse"
                      }
                    , { label: "Haus"
                      , value: TText "detachedHouse"
                      }
                    ]
            )
        , Dynamic "financingProfile" "Optionale Beschreibung der Kundenbedürfnisse" (LineDefinition "")
        , Fixed (Paragraph "* benötigte Felder")
        ]
    }
