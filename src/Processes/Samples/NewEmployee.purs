module  Processes.Samples.NewEmployee where

import Processes.AppPrelude (($))
import Processes.Model.Types (Snapshot, Transition, Transitions, User, Process)
import Data.List (fromFoldable)
import Processes.Form.Types (BasicValue(..), FormDefinition(..), FormEntryDefinition(..), FormEntryDefinitionDynamic(..), FormEntryDefinitionFixed(..))
import Data.Map as MS
import Data.Tuple (Tuple(..))

process :: Process
process =
  { snapshot: initialSnapshot
  , name: "New Employee"
  , users: fromFoldable [ defaultUser, bossUser ]
  }

defaultUser :: User
defaultUser =
  { userId: "joris"
  , userRoles: fromFoldable [ "new-employee" ]
  }

bossUser :: User
bossUser =
  { userId: "dandolo"
  , userRoles: fromFoldable [ "admin" ]
  }

initialSnapshot :: Snapshot
initialSnapshot =
  { currentState: "root"
  , transitions: transitions
  , data:
    MS.fromFoldable
      []
  }

transitions :: Transitions
transitions =
  MS.fromFoldable
    [ Tuple "add-email" root
    , Tuple "employee-data" addEmployeeData
    ]

root :: Transition
root =
  { origin: "root"
  , exit: "add-employee-data"
  , request:
    { formDefinition: requestForm
    }
  , validation:
    { checkRoles: fromFoldable [ "admin" ]
    , requiredFields: fromFoldable [ "email" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    }
  }

addEmployeeData :: Transition
addEmployeeData =
  { origin: "add-employee-data"
  , exit: "exit"
  , request:
    { formDefinition: addEmployeeDataForm
    }
  , validation:
    { checkRoles: fromFoldable [ "new-employee" ]
    , requiredFields: fromFoldable [ "ahv", "name" ]
    , optionalFields: fromFoldable [ "coffee" ]
    , prerequiredFields: fromFoldable []
    }
  }

addEmployeeDataForm :: FormDefinition
addEmployeeDataForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Welcome to our team")
        , Fixed (Paragraph "Please fill out the following form to finish the onboarding")
        , Dynamic "ahv" "Your AHV-Number" (LineDefinition "756.9217.0769.85")
        , Dynamic "name" "Your name" (LineDefinition "Joris Morger")
        , Dynamic "coffee" "Do you drink coffe?"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "yes"
                      , value: TText "yes"
                      }
                    , { label: "no"
                      , value: TText "no"
                      }
                    ]
            )
        ]
    }

requestForm :: FormDefinition
requestForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Please kick off the onboarding by providing the email of the new employee")
        , Dynamic "email" "E-Mail" (LineDefinition "employee@gmail.com")
        ]
    }
