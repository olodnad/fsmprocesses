module Processes.UI where

import Prelude
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Data.Maybe (Maybe)
import Data.Formatter.Number as NF

showRoundCurrency :: Number -> String
showRoundCurrency n = NF.format (NF.Formatter { abbreviations: false, after: 0, before: 0, comma: true, sign: false }) n <> " CHF"

showRound :: Number -> String
showRound n = NF.format (NF.Formatter { abbreviations: false, after: 0, before: 0, comma: true, sign: false }) n

mkClass ::
  forall i r.
  String -> HP.IProp ( class :: String | r ) i
mkClass c = HP.class_ $ HH.ClassName c

inputGroup :: forall p i. String -> String -> (String -> Maybe i) -> String -> HH.HTML p i
inputGroup label value onInput error =
  HH.div
    [ mkClass "input-group input-group-sm mb-3" ]
    [ HH.div
        [ mkClass "input-group-prepend" ]
        [ HH.span
            [ mkClass "input-group-text" ]
            [ HH.text label ]
        ]
    , HH.input
        [ HP.value value
        , HE.onValueInput onInput
        , mkClass "form-control"
        ]
    , HH.text error
    ]
