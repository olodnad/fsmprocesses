module Processes.App where

-- import Debug.Trace
import Processes.AppPrelude
import Data.List (fromFoldable, List, toUnfoldable)
import Data.Maybe (Maybe(..))
import Data.Symbol (SProxy(..))
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Processes.Model.Types (Process, ProcessName)
import  Processes.Samples.Hardware as HardwareSample
import  Processes.Samples.DIS as DISSample
import  Processes.Samples.NewEmployee as NewEmployeeSample
import  Processes.Samples.HypoOffer as HypoOfferSample

import Sections.Process as CSM
import Processes.UI (mkClass)

data Action
  = UpdateProcessState CSM.ProcessState
  | SetCurrentProcess ProcessName

type State
  = { process :: Maybe CSM.ProcessState
    , currentProcess :: ProcessName
    }

type ChildSlots
  = ( process :: CSM.Slot ProcessName
    )

_process = SProxy :: SProxy "process"

component :: forall q i o. H.Component HH.HTML q i o Aff
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

initialState :: forall i. i -> State
initialState _ =
  { process: Nothing
  , currentProcess: HypoOfferSample.process.name
  }

sampleProcesses :: List Process
sampleProcesses = fromFoldable [ HardwareSample.process, NewEmployeeSample.process, DISSample.process, HypoOfferSample.process ]

render :: State -> H.ComponentHTML Action ChildSlots Aff
render state =
  HH.div
    [ mkClass "container mt-5" ]
    [ HH.nav_
        [ HH.div [ mkClass "nav nav-tabs", HP.id_ "nav-tab" ]
            ( toUnfoldable
                $ map
                    ( \process ->
                        HH.a
                          [ mkClass $ "nav-item nav-link" <> if process.name == state.currentProcess then " active" else ""
                          , HE.onClick $ (\_ -> Just $ SetCurrentProcess process.name)
                          ]
                          [ HH.text process.name ]
                    )
                    sampleProcesses
            )
        ]
    , HH.div
        [ mkClass "tab-content" ]
        ( toUnfoldable
            $ map
                ( \process ->
                    HH.div
                      [ mkClass $ "tab-pane fade" <> if process.name == state.currentProcess then " show active" else "" ]
                      [ HH.slot _process process.name CSM.component process (\processState -> Just $ UpdateProcessState processState) ]
                )
                sampleProcesses
        )
    ]

handleAction :: forall o m. MonadAff m => Action -> H.HalogenM State Action ChildSlots o m Unit
handleAction = case _ of
  UpdateProcessState process -> do
    currentState <- H.get
    H.put { process: Just process, currentProcess: currentState.currentProcess }
  (SetCurrentProcess processName) -> do
    H.modify_ $ _ { currentProcess = processName }
