{ pkgs ? import ./nix/nixpkgs.nix {} }:
let
  easyPS = pkgs."easy-purescript-nix";
in pkgs.stdenv.mkDerivation rec {
    name = "zprocess";
    env = pkgs.buildEnv { name = name; paths = buildInputs; };
    buildInputs = [
        easyPS.spago
        easyPS.purescript
        pkgs.nodejs
        pkgs.git
        pkgs.nodePackages.parcel-bundler
    ];
}
